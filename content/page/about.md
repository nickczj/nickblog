---
title: About Nick
comments: false
---

I'm a Software Engineer trying to get good at his job and I place my thoughts here sometimes. 

My interests include math, physics and story telling.